<?php

namespace App\Http\Controllers\api;

#Basic
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
#resources
use App\Http\Resources\bankResource;
use App\Http\Resources\notificationResource;
use App\Http\Resources\pageResource;
use App\Http\Resources\sectionResource;
use App\Http\Resources\sliderResource;
use App\Http\Resources\sub_sectionResource;
#config->app
use Validator;
use Auth;
use Hash;
#Mail
use Mail;
use App\Mail\ActiveCode;
#Models
use App\Models\Role;
use App\Models\Section;
use App\Models\Notification;
use App\Models\Page;
use App\Models\Bank_account;
use App\Models\Bank_transfer;
use App\Models\Contact;
use App\Models\Device;
use App\Models\Slider;
use App\Models\Sub_section;
use App\User;

class apiController extends Controller
{

    /*
    |----------------------------------------------------|
    |                Home Page Start                     |
    |----------------------------------------------------|
    */

    #home
    public function home(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'nullable|exists:users,id',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        $data = [
            'sliders'  => sliderResource::collection(Slider::get()),
            'sections' => sectionResource::collection(Section::get()),
            'services' => [],
        ];
        #success response
        return api_response(1, trans('api.send'), $data, ['notification_count' => user_notify_count($request->user_id)]);
    }

    /*
    |----------------------------------------------------|
    |                 data Page Start                    |
    |----------------------------------------------------|
    */

    #show section
    public function showSection(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'nullable|exists:users,id',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        #success response
        return api_response(1, trans('api.send'), sectionResource::collection(Section::get()), ['notification_count' => user_notify_count($request->user_id)]);
    }

    #show sub_section
    public function showSubSection(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'user_id'    => 'nullable|exists:users,id',
            'section_id' => 'required|exists:sections,id',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        #success response
        return api_response(1, trans('api.send'), sub_sectionResource::collection(Sub_section::whereSectionId($request->section_id)->get()), ['notification_count' => user_notify_count($request->user_id)]);
    }

    /*
    |----------------------------------------------------|
    |                static Page Start                   |
    |----------------------------------------------------|
    */

    #page (About us , Conditions)
    public function page(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'nullable|exists:users,id',
            'title'   => 'required|in:about,condition',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        #success response
        return api_response(1, trans('api.send'), new pageResource(Page::where('title_en', 'like', '%' . $request->title . '%')->first()), ['notification_count' => user_notify_count($request->user_id)]);
    }

    #contact us
    public function contactUs(Request $request)
    {
        #store contact us
        $request->request->add(['seen' => '0']);
        Contact::create($request->except(['lang']));

        #success response
        return api_response(1, trans('api.send'));
    }

    /*
    |----------------------------------------------------|
    |               Bank Page Start                      |
    |----------------------------------------------------|
    */

    #bank Account
    public function bankAccount(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'nullable|exists:users,id',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        #success response
        return api_response(1, trans('api.send'), bankResource::collection(Bank_account::get()), ['notification_count' => user_notify_count($request->user_id)]);
    }

    #bank Transfer
    public function bankTransfer(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        #bank transfer
        Bank_transfer::create($request->except(['lang']));

        #success response
        return api_response(1, trans('api.send'));
    }

    /*
    |----------------------------------------------------|
    |           Notification Page Start                  |
    |----------------------------------------------------|
    */

    #show Notification
    public function showNotification(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        #update seen
        Notification::whereToId($request->user_id)->update(['seen' => 1]);

        #success response
        return api_response(1, trans('api.send'), notificationResource::collection(Notification::whereToId($request->user_id)->orderBy('id', 'desc')->get()), ['notification_count' => user_notify_count($request->user_id)]);
    }

    #delete Notification
    public function deleteNotification(Request $request)
    {
        #validation
        $validator = Validator::make($request->all(), [
            'notification_id' => 'nullable|exists:notifications,id',
        ]);

        #error response
        if ($validator->fails()) return api_response(0, $validator->errors()->first());

        #delete notification
        Notification::whereId($request->notification_id)->delete();

        #success response
        return api_response(1, trans('api.delete'));
    }
}
