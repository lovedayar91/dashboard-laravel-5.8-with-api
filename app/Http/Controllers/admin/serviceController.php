<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Validator;
use App\Models\Service;
use Illuminate\Http\Request;

class serviceController extends Controller
{
    #index
    public function index()
    {
        $data = Service::get();
        return view('dashboard.services', compact('data'));
    }

    #store
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'section_id'        => 'required|exists:sections,id',
            'title_ar'          => 'required|max:255',
            'title_en'          => 'nullable|max:255',
            'short_desc_en'     => 'nullable',
            'short_desc_en'     => 'nullable',
            'desc_en'           => 'nullable',
            'desc_en'           => 'nullable',
            'price'             => 'nullable',
            'amount'            => 'nullable',
            'image'             => 'nullable|image',
        ]);

        #error response
        if ($validator->fails())
            return response()->json(['value' => 0, 'msg' => $validator->errors()->first()]);

        #image
        if ($request->hasFile('photo')) $request->request->add(['image' => upload_image($request->file('photo'), 'public/images/sections')]);
        #store new service
        $service = Service::create($request->except(['_token', 'photo']));

        #add adminReport
        admin_report('أضافة الخدمة ' . $request->title_ar);

        #success response
        session()->flash('success', awtTrans('تم الحفظ بنجاح'));
        return response()->json(['value' => 1, 'msg' => awtTrans('تم الحفظ بنجاح')]);
    }

    #update
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'section_id'        => 'required|exists:sections,id',
            'title_ar'          => 'required|max:255',
            'title_en'          => 'nullable|max:255',
            'short_desc_en'     => 'nullable',
            'short_desc_en'     => 'nullable',
            'desc_en'           => 'nullable',
            'desc_en'           => 'nullable',
            'price'             => 'nullable',
            'amount'            => 'nullable',
            'image'             => 'nullable|image',
        ]);

        #error response
        if ($validator->fails())
            return response()->json(['value' => 0, 'msg' => $validator->errors()->first()]);

        #image
        if ($request->hasFile('photo')) $request->request->add(['image' => upload_image($request->file('photo'), 'public/images/sections')]);
        #update service
        $service = Service::whereId($request->id)->first();
        $service->update($request->except(['_token', 'photo']));

        #add adminReport
        admin_report('تعديل الخدمة ' . $request->title_ar);

        #success response
        session()->flash('success', awtTrans('تم التعديل بنجاح'));
        return response()->json(['value' => 1, 'msg' => awtTrans('تم التعديل بنجاح')]);
    }

    #delete one
    public function delete(Request $request)
    {
        #get service
        $service = Service::whereId($request->id)->firstOrFail();
        $title_ar = $service->title_ar;

        #send FCM

        #delete service
        $service->delete();

        #add adminReport
        admin_report('حذف الخدمة ' . $title_ar);

        #success response
        return back()->with('success', awtTrans('تم الحذف'));
    }

    #delete more than one or all
    public function delete_all(Request $request)
    {
        $type = $request->type;
        #get services
        if ($type == 'all') $services = Service::get();
        else {
            $ids = $request->service_ids;
            $first_ids   = ltrim($ids, ',');
            $second_ids  = rtrim($first_ids, ',');
            $service_ids = explode(',', $second_ids);
            $services = Service::whereIn('id', $service_ids)->get();
        }

        foreach ($services as $service) {
            #send FCM

            #delete service
            $service->delete();
        }

        #add adminReport
        admin_report('حذف اكتر من خدمة');

        #success response
        return back()->with('success', awtTrans('تم الحذف'));
    }
}
