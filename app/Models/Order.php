<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];

    // user
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // section
    public function section()
    {
        return $this->belongsTo('App\Models\Section');
    }

    // order_items
    public function order_items()
    {
        return $this->hasMany('App\Models\Order_item');
    }

    #get section_title
    public function getSectionTitleAttribute()
    {
        $attr = 'section_title_' . App::getLocale();
        return $this->attributes[$attr];
    }
}
